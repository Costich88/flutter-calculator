import 'dart:math' as math;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Calc',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Calculator'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Color outupcolor = Colors.black45;
  Color expcolor = Colors.black;
  bool inv = false;
  List funclist = ['sin', 'cos', 'tan', 'ln', 'log', 'sqrt'];
  String output = "";
  String expression = "0";
  bool num = true;
  bool point = false;

  List directfunc = ['sin', 'cos', 'tan', 'ln', 'log', 'sqrt'];
  List invfunc = ['asin', 'acos', 'atan', 'e^', '10^', '^2'];

  buttonPressed(String buttonText) {

      print(buttonText);
      print(expression);
      if (buttonText == "CL") {
        output = "";
        expression = "0";
        num = false;
        point = false;
      } else if (buttonText == "+" || buttonText == "/" || buttonText == "*") {
        if (num) {
          expression = expression + buttonText;
          num = false;
          point = false;
        }
      } else if (buttonText == ".") {
        if (!point) {
          expression += buttonText;
          point = true;
        }
      } else if (buttonText == "sin" ||
          buttonText == "cos" ||
          buttonText == "ln" ||
          buttonText == "tan") {
        if (expression == "0")
          expression = buttonText + "(";
        else
          expression += buttonText + "(";
      } else if (buttonText == "^" || buttonText == "sqrt" || buttonText == "(") {
        if (expression == "0")
          expression = buttonText;
        else
          expression += buttonText;
      } else if (buttonText == "log" ||
          buttonText == "asin" ||
          buttonText == "acos" ||
          buttonText == "atan") {
        try {
          Parser p = new Parser();

          Expression exp = p.parse(expression);

          ContextModel cm = new ContextModel();

          double temp = exp.evaluate(EvaluationType.REAL, cm);

          switch (buttonText) {
            case "log":
              output = math.log(temp).toString();
              break;
            case "asin":
              output = math.asin(temp).toString();
              break;
            case "acos":
              output = math.acos(temp).toString();
              break;
            case "atan":
              output = math.atan(temp).toString();
              break;
          }
          expression = buttonText + "(" + expression + ")";
        } catch (e) {
          expression += buttonText;
          output = "";
        }
      } else if (buttonText == "!" || buttonText == "%") {
        try {
          Parser p = new Parser();

          Expression exp = p.parse(expression);

          ContextModel cm = new ContextModel();

          double temp = exp.evaluate(EvaluationType.REAL, cm);

          if (buttonText == "%") {
            expression += buttonText;
            output = (temp / 100).toString();
          } else {
            if (temp % 1 == 0) {
              int inttemp = temp.round();

              int factorial(inttemp) {
                if (inttemp <= 0) {
                  // termination case
                  return 1;
                } else {
                  return (inttemp * factorial(inttemp - 1));
                  // function invokes itself
                }
              }

              output = factorial(inttemp).toString();
              expression += buttonText;
            }
          }
        } catch (e) {
          expression += buttonText;
          output = "";
        }
      } else if (buttonText == "=") {
        if (output == "" && expression != "0") {
          output = "Bad expression";
          outupcolor = Colors.red;
          expcolor = Colors.red;
        } else {
          expression = output;
          output = "";
          point = false;
        }
      } else {

    if (buttonText == "INV") {
      inv = !inv;
      if (inv) {
        funclist = invfunc;
      } else
        funclist = directfunc; }

      if (buttonText == "pi") {
        buttonText = math.pi.toStringAsFixed(5);
      }

      if (buttonText == "e") {
        buttonText = math.pi.toStringAsFixed(5);
      }

      if (expression == "0")
        expression = buttonText;
      else
        expression += buttonText;
      num = true;

      setState(() {
        try {
          outupcolor = Colors.black45;
          expcolor = Colors.black;

          Parser p = new Parser();

          Expression exp = p.parse(expression);

          ContextModel cm = new ContextModel();

          output = exp.evaluate(EvaluationType.REAL, cm).toString();

          if (output.length > 12) {
            output =
                double.parse(output).toStringAsFixed(8);
          }

          if (double.parse(output) % 1 == 0) {
            output =
                double.parse(output).round().toString();
          }
        } catch (e) {
          output = "";
        }
      });

      }
    setState(() {
    });
  }

  Widget buildButton(String buttonText) {
    return new Expanded(
      child: new FlatButton(
        padding: EdgeInsets.symmetric(
          horizontal: 0.09 * MediaQuery.of(context).size.width,
        ),
        child: new Text(
          buttonText,
          style: TextStyle(
            fontSize: 0.09 * MediaQuery.of(context).size.width,
          ),
        ),
        color: Color.fromRGBO(1, 1, 1, 0.0),
        onPressed: () => buttonPressed(buttonText),
      ),
    );
  }

  Widget buildFuncButton(String buttonText) {
    return new Container(
      width: 0.3 * MediaQuery.of(context).size.width,
      height: 0.115 * MediaQuery.of(context).size.height,
      child: new FlatButton(
        child: new Text(
          buttonText,
          style: TextStyle(
            fontSize: 0.06 * MediaQuery.of(context).size.width,
          ),
        ),
        color: Color.fromRGBO(1, 1, 1, 0.0),
        textColor: Colors.white,
        onPressed: () => buttonPressed(buttonText),
      ),
    );
  }

  Widget buildExecButton(String buttonText) {
    return new Expanded(
      child: new FlatButton(
        padding: EdgeInsets.symmetric(
          horizontal: 0.09 * MediaQuery.of(context).size.width,
        ),
        child: new Text(
          buttonText,
          style: TextStyle(
            fontSize: 0.075 * MediaQuery.of(context).size.width,
          ),
        ),
        color: Color.fromRGBO(1, 1, 1, 0.0),
        textColor: Color.fromRGBO(51, 210, 45, 100.0),
        onPressed: () => buttonPressed(buttonText),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Container(
            decoration:
                new BoxDecoration(color: Color.fromRGBO(242, 243, 245, 100.0)),
            child: new Column(
              children: <Widget>[
                new Container(
                    height: 0.1 * MediaQuery.of(context).size.height,
                    color: Colors.white,
                    padding: new EdgeInsets.fromLTRB(
                        0.08 * MediaQuery.of(context).size.width,
                        0.05 * MediaQuery.of(context).size.height,
                        0.08 * MediaQuery.of(context).size.width,
                        0.0),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'RAD',
                      style: TextStyle(
                        color: Colors.black45,
                        fontSize: 0.03 * MediaQuery.of(context).size.height,
                      ),
                    )),
                new Container(
                  height: 0.15 * MediaQuery.of(context).size.height,
                  color: Colors.white,
                  alignment: Alignment.centerRight,
                  padding: new EdgeInsets.symmetric(
                    horizontal: 0.05 * MediaQuery.of(context).size.width,
                  ),
                  child: AutoSizeText(
                    expression,
                    style: new TextStyle(
                      fontSize: 0.2 * MediaQuery.of(context).size.width,
                      color: expcolor,
                    ),
                    maxLines: 1,
                    minFontSize: 0.1 * MediaQuery.of(context).size.width,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                new Container(
                  height: 0.15 * MediaQuery.of(context).size.height,
                  alignment: Alignment.centerRight,
                  color: Colors.white,
                  padding: new EdgeInsets.symmetric(
                    vertical: 0.03 * MediaQuery.of(context).size.height,
                    horizontal: 0.05 * MediaQuery.of(context).size.width,
                  ),
                  child: AutoSizeText(
                    output,
                    style: new TextStyle(
                      fontSize: 0.105 * MediaQuery.of(context).size.width,
                      color: outupcolor,
                    ),
                    maxLines: 1,
                  ),
                ),
                new Container(
                  height: 3.0,
                  color: Colors.black12,
                ),
                new Expanded(
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Column(
                        children: [
                          buildButton("7"),
                          buildButton("4"),
                          buildButton("1"),
                          buildButton("0"),
                        ],
                      ),
                      Column(
                        children: [
                          buildButton("8"),
                          buildButton("5"),
                          buildButton("2"),
                          buildButton("."),
                        ],
                      ),
                      Column(
                        children: [
                          buildButton("9"),
                          buildButton("6"),
                          buildButton("3"),
                          buildButton("="),
                        ],
                      ),
                      Container(
                        color: Colors.black12,
                        width: 2.0,
                      ),
                      Column(
                        children: [
                          buildExecButton("CL"),
                          buildExecButton("/"),
                          buildExecButton("*"),
                          buildExecButton("-"),
                          buildExecButton("+"),
                        ],
                      ),
                      Container(
                        color: Color.fromRGBO(51, 210, 45, 100.0),
                        child: Column(
                          children: [
                            buildFuncButton("INV"),
                            buildFuncButton(funclist[0]),
                            buildFuncButton(funclist[3]),
                            buildFuncButton("pi"),
                            buildFuncButton("("),
                          ],
                        ),
                      ),
                      Container(
                        color: Color.fromRGBO(51, 210, 45, 100.0),
                        child: Column(
                          children: [
                            buildFuncButton("%"),
                            buildFuncButton(funclist[1]),
                            buildFuncButton(funclist[4]),
                            buildFuncButton("e"),
                            buildFuncButton(")"),
                          ],
                        ),
                      ),
                      Container(
                        color: Color.fromRGBO(51, 210, 45, 100.0),
                        child: Column(
                          children: [
                            buildFuncButton(""),
                            buildFuncButton(funclist[2]),
                            buildFuncButton(funclist[5]),
                            buildFuncButton("^"),
                            buildFuncButton("!"),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )));
  }
}
