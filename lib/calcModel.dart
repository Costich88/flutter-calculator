import 'dart:math' as math;
import 'package:math_expressions/math_expressions.dart';
import 'package:flutter/material.dart';

class CalcModel {

  String output = "";
  String expression = "1";
  bool num = true;
  bool point = false;
  Color outupcolor = Colors.black45;
  Color expcolor = Colors.black;

  List directfunc = ['sin', 'cos', 'tan', 'ln', 'log', 'sqrt'];
  List invfunc = ['asin', 'acos', 'atan', 'e^', '10^', '^2'];

  buttonsActions(String buttonText) {
    print(buttonText);
    print(expression);
    if (buttonText == "CL") {
      output = "";
      expression = "1";
      num = false;
      point = false;
    } else if (buttonText == "+" || buttonText == "/" || buttonText == "*") {
      if (num) {
        expression = expression + buttonText;
        num = false;
        point = false;
      }
    } else if (buttonText == ".") {
      if (!point) {
        expression += buttonText;
        point = true;
      }
    } else if (buttonText == "sin" ||
        buttonText == "cos" ||
        buttonText == "ln" ||
        buttonText == "tan") {
      if (expression == "0")
        expression = buttonText + "(";
      else
        expression += buttonText + "(";
    } else if (buttonText == "^" || buttonText == "sqrt" || buttonText == "(") {
      if (expression == "0")
        expression = buttonText;
      else
        expression += buttonText;
    } else if (buttonText == "log" ||
        buttonText == "asin" ||
        buttonText == "acos" ||
        buttonText == "atan") {
      try {
        Parser p = new Parser();

        Expression exp = p.parse(expression);

        ContextModel cm = new ContextModel();

        double temp = exp.evaluate(EvaluationType.REAL, cm);

        switch (buttonText) {
          case "log":
            output = math.log(temp).toString();
            break;
          case "asin":
            output = math.asin(temp).toString();
            break;
          case "acos":
            output = math.acos(temp).toString();
            break;
          case "atan":
            output = math.atan(temp).toString();
            break;
        }
        expression = buttonText + "(" + expression + ")";
      } catch (e) {
        expression += buttonText;
        output = "";
      }
    } else if (buttonText == "!" || buttonText == "%") {
      try {
        Parser p = new Parser();

        Expression exp = p.parse(expression);

        ContextModel cm = new ContextModel();

        double temp = exp.evaluate(EvaluationType.REAL, cm);

        if (buttonText == "%") {
          expression += buttonText;
          output = (temp / 100).toString();
        } else {
          if (temp % 1 == 0) {
            int inttemp = temp.round();

            int factorial(inttemp) {
              if (inttemp <= 0) {
                // termination case
                return 1;
              } else {
                return (inttemp * factorial(inttemp - 1));
                // function invokes itself
              }
            }

            output = factorial(inttemp).toString();
            expression += buttonText;
          }
        }
      } catch (e) {
        expression += buttonText;
        output = "";
      }
    } else if (buttonText == "=") {
      if (output == "" && expression != "0") {
        output = "Bad expression";
        outupcolor = Colors.red;
        expcolor = Colors.red;
      } else {
        expression = output;
        output = "";
        point = false;
      }
    }

    }

  }